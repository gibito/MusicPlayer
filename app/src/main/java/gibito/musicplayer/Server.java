package gibito.musicplayer;

import android.content.res.AssetManager;
import android.media.MediaMetadata;
import android.media.MediaMetadataRetriever;

import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

import static fi.iki.elonen.NanoHTTPD.Response.Status;


class Server extends NanoHTTPD {
    private AssetManager assetManager;
    private MusicDB db;
    private Player player;

    Server(int port, AssetManager assetManager, MusicDB db, Player player) throws IOException {
        super(port);
        this.assetManager = assetManager;
        this.db = db;
        this.player = player;
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    }

    @Override
    public Response serve(IHTTPSession session) {
        if (session.getParameters().isEmpty())
            return getFileFromAssets(session);
        else
            return getWithParams(session);
    }


    private Response getWithParams(IHTTPSession session) {
        Map<String, List<String>> params = session.getParameters();

        if (params.containsKey("status"))
            return getStatus();
        if (params.containsKey("song"))
            return getSong(params.get("song").get(0));
        if (params.containsKey("pic"))
            return getPic(params.get("pic").get(0));
        if (params.containsKey("list"))
            return getList();

        return newFixedLengthResponse(Status.NOT_IMPLEMENTED, "text/plain", "Unknown parameter(s): " + params);
    }

    private Response getSong(String name) {
        try {
            File song = db.openFromDB(name);
            if (song != null && song.exists())
                return newFixedLengthResponse(Status.OK, "audio/mpeg", new FileInputStream(song), song.length());
        } catch (IOException ignored) {
        }
        return newFixedLengthResponse(Status.NOT_FOUND, "text/plain", "Song not found");
    }

    private Response getPic(String name) {
        File song = db.openFromDB(name);
        if (song != null && song.exists()) {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(song.getPath());
            byte[] pic = mmr.getEmbeddedPicture();
            if (pic != null)
                return newFixedLengthResponse(Status.OK, "image", new ByteArrayInputStream(pic), pic.length);
        }
        return newFixedLengthResponse(Status.NOT_FOUND, "text/plain", "Picture not found");

    }

    private Response getList() {
        return newFixedLengthResponse(new Gson().toJson(db.listDB()));
    }

    private Response getStatus() {
        return newFixedLengthResponse(new Gson().toJson(player.getStatus()));
    }

    private Response getFileFromAssets(IHTTPSession session) {
        String link = session.getUri();
        link = link.substring(1, link.length());
        // Load file
        try {
            // Select MIME type
            String type = "text/plain";
            if (link.endsWith(".mp3")) type = "audio/mpeg";
            if (link.endsWith(".ico")) type = "image/x-icon";
            if (link.endsWith(".css")) type = "text/css";
            if (link.endsWith(".html")) type = "text/html";

            return newChunkedResponse(Status.OK, type, assetManager.open(link));
        } catch (IOException e) {
            // Load 404
            try {
                System.out.println(link + " not found!");
                return newChunkedResponse(Status.NOT_FOUND, "html", assetManager.open("notfound.html"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return newFixedLengthResponse("Internal Error!");
    }


}

