package gibito.musicplayer;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class MusicDB {
    private String dbPath;

    class DB {
        private ArrayList<String> dirs;
        private HashMap<String, Integer> names;

        DB() {
            dirs = new ArrayList<>();
            names = new HashMap<>();
        }
    }

    private DB db;

    MusicDB(String filesDir) {
        dbPath = filesDir + "/json/db.json";
        db = new DB();
        try {
            if (dbExists())
                openDB();
            else createDB();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void scan() {
        /// Remove old
        String[] names = new String[db.names.size()];
        db.names.keySet().toArray(names);
        for (String name : names) {
            if (!new File(db.names.get(name) + name).exists())
                db.names.remove(name);
        }

        /// Add new
        for (int i = 0; i < db.dirs.size(); i++) {
            File dir = new File(db.dirs.get(i));
            for (String name : dir.list()) {
                if (name.endsWith(".mp3") && !db.names.containsKey(name))
                    db.names.put(name, i);
            }
        }
    }

    void createDB() throws IOException {
        scan();
        writeDB();
    }

    private void writeDB() throws IOException {
        FileWriter fw = new FileWriter(dbPath);
        fw.write(new Gson().toJson(db));
        fw.close();
    }

    private void openDB() throws FileNotFoundException {

        Scanner scanner = new Scanner(new File(dbPath));
        StringBuilder json = new StringBuilder();
        while (scanner.hasNextLine())
            json.append(scanner.nextLine());

        db = new Gson().fromJson(json.toString(), db.getClass());
    }

    private boolean dbExists() throws IOException {
        File file = new File(dbPath);
        boolean ex = file.exists();
        if (!ex) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        return ex;
    }

    File openFromDB(String name) {
        File song = new File(db.dirs.get(db.names.get(name)));
        if (song.exists())
            return song;

        db.names.remove(name);
        try {
            writeDB();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    String[] listDB() {
        String[] names = new String[db.names.size()];
        db.names.keySet().toArray(names);
        return names;
    }

    String[] listDirs() {
        String[] dirs = new String[this.db.dirs.size()];
        this.db.dirs.toArray(dirs);
        return dirs;
    }

    void addDir(String dir) {
        db.dirs.add(dir);

        File folder = new File(dir);
        for (String name : folder.list()) {
            if (name.endsWith(".mp3") && !db.names.containsKey(name))
                db.names.put(name, db.dirs.indexOf(dir));
        }

        String[] subDirs = new File(dir).list(
                new FilenameFilter() {
                    @Override
                    public boolean accept(File current, String name) {
                        return new File(current, name).isDirectory();
                    }
                }
        );

        for (String subDir : subDirs)
            addDir(dir + "/" + subDir);

        // Is it the root of recursion
        if (Thread.currentThread().getStackTrace().length == 1 ||
                !Thread.currentThread().getStackTrace()[0].getMethodName().
                        equals(Thread.currentThread().getStackTrace()[1].getMethodName())) {
            try {
                writeDB();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void removeDir(String dir) {
        String songs[] = listDB();

        for (String song : songs)
            if (db.dirs.get(db.names.get(song)).equals(dir))
                db.names.remove(song);

        for (String path : db.dirs)
            if (path.startsWith(dir))
                db.dirs.remove(path);
    }


}
