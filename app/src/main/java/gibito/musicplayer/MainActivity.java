package gibito.musicplayer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    MusicDB db;
    Player player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    0);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }


        db = new MusicDB(getFilesDir().getAbsolutePath());
        player = new Player(getFilesDir().getAbsolutePath());
//        db.addDir(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Music");

        ListView listView = findViewById(R.id.list);
        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, db.listDB()));

        try {
            new Server(8080, getAssets(), db, player);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0) {
            switch (requestCode) {
                case 0: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        System.out.println("Read permission granted");
                    else
                        System.out.println("Read permission denied");
                }
                case 1: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                        System.out.println("Write permission granted");
                    else
                        System.out.println("Write permission denied");
                }
            }
        }
    }


    public void updateClick(View view) {
        try {
            db.createDB();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, db.listDB()));
    }
}
