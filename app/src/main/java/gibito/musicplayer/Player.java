package gibito.musicplayer;

public class Player {
    class Status {
        boolean playing;
        int position;

        String[] playlist;
        int currentSong;

        boolean repeat;
        boolean random;
    }

    private String settingsPath;
    private Status status;

    Player(String filesDir) {
        settingsPath = filesDir + "/json/settings.json";

        status = new Status();

        status.playing = false;
        status.position = 88;
        status.playlist = new String[]{"song1", "song2", "song3", "song4"};
        status.currentSong = 2;
    }

    private boolean settingsExists() {
        return false;
    }


    public Status getStatus() {
        return status;
    }
}
