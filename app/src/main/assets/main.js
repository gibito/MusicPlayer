var song = new Audio();
var playing = false;

function downloadSong(name) {
    song = new Audio("/?song=" + name);
}

function playButton() {
    if (playing) {
        song.pause();
        playing = false;
        document.getElementById("pb").innerHTML = "Play";
       } else {
        song.play();
        playing = true;
        document.getElementById("pb").innerHTML = "Pause";
       }
}

function volume() {
    song.volume = document.getElementById("vol").value / 100;
}